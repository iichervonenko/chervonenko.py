# HW-2
# Дать возможность пользователю ввести данные, приобразовать их в set, list, tuple, dict
# (тут нужно будет посмотреть докоментацию по dict) и
# вывести результат (вывод делать после каждого приобразования)

# Name = input('Your_name: ')
# print(Name)
# print(type(Name))
#
# s = set(Name)
# print(s)
# print(type(s))
#
# l = list(Name)
# print(l)
# print(type(l))
#
# t = tuple(Name)
# print(t)
# print(type(t))
#
# d1 = dict.fromkeys(l, )
# print(d1)
# print(type(d1))
#
# num = len(l)
# l2 = [i for i in range(1, num+1)]
# print(l2)
#
# d2 = dict(zip(l, l2))
# print(d2)
# print(type(d2))

# HW-3

# 1. Валидатор паролей.
#
# while True:
#     passw = input('Enter password: ')
#     if all([len(passw) < 6 or len(passw) > 16]):
#         print('Invalid 1. В пароле должно быть от 6 до 16 символов')
#     n = len(passw)
#     if sum(bool(p.isdigit()) for p in passw) < 1:
#        print('Invalid 2. В пароле должны использоваться цифры')
#     elif sum(bool(p.isalpha()) for p in passw) < 1:
#         print('Invalid 3. В пароле должны использоваться буквы')
#     elif sum(bool(p.islower()) for p in passw) < 1:
#         print('Invalid 4. В пароле должны использоваться буквы в нижнем регистре')
#     elif sum(bool(p.isupper()) for p in passw) < 1:
#         print('Invalid 5. В пароле должны использоваться буквы в высоком регистре')
#     else:
#         break
#         print('Valid')
#     continue
# while True:
#     passw2 = input('Repeat password: ')
#     if passw == passw2:
#         print('Congratulations, your password is valid')
#     else:
#          continue
#          print('Invalid 6. Пароли не совпадают')
#     break
# 3. Нарисовать в консоли ёлочку

# m = input('Введите число: ')
# n = int(m)
# for i in range(n, 0, -1):
#     print("_" * (i-1), end="")
#     print("*" * (((n - i) * 2) + 1))

# 2. Создать список, где все элементы будут кратные 5ти (упражнение на функцию range)
# 4. Задать число и посчитать количество цифр в нем
# 5. Сгенерировать произваольный список и развернуть его

# num = str(n ** n)
# num = input('Enter your number: ')
# if num.isdigit():
#     print('The Christmas Tree has ', len(num), ' rows')
# else:
#     print('Not a number')
# a = len(num)
#
# for b in range(a, (a+1)**2, ):
#     if not b % 5:
#         print (b)
#
# list = [i for i in range(1, a)]
# print(list[::-1])

"""
HW4
"""
"""
1. Написать любую детерменированную функцию (Детерменированная функция = функция, которая возвращает одно и тоже вне
зависимости от парамеметров)
"""

# def square(a,b):
#     return a * b
#
# print(square(3,5))

"""
2. Написать функцию, которая вернет True если число четное и False если не четное
"""

# def even_number(num):
#     res = True if num % 2 == 0 else False
#     return res
#
# print(even_number(4))

"""
3. Напишите функция is_prime, которая принимает 1 аргумент (число) и возвращает True, если число простое, иначе False
Простое число - это число, которое делится без остатка только на себя и на 1
"""

# def is_prime(s):
#     num = False
#     for r in range(2, s):
#         if s % r == 0:
#             num = True
#             break
#     if num:
#         return False
#     else:
#         return True
#
# print(is_prime(121))

"""
4. Напишите функцию, которая принимает 1 аргумент (строка) и выполняет следующие действия на каждую из букв строки:
i - инкремент (+1)
d - дикремент (-1)
s - возведение в квадрат
o - добавить число в результативный список
остальные буквы игнорируются
Исходное число = 0
Результативный список = []
Вернуть результативный список
parse("iiisdoso")  ==>  [8, 64] <- это как пример
"""
# def parse(operation):
#     num = 7
#     res = []
#     for op in operation:
#         if op == 'i':
#             num += 1
#         elif op == 'd':
#             num -= 1
#         elif op == 's':
#             num **= 2
#         elif op == 'o':
#             res.append(num)
#     return res
# parse("iiisdoso")
# print(parse("iiisdoso"))
# assert parse("iiisdoso") == [99, 9801]

"""
5. Написать функцию, которая из строки делает datetime и возвращает результат, если введенный 
формат даты неверный - возвращать None. Аргументы - срока-дата, формат, 
по которому можно привести строку в datetime
"""

# from datetime import datetime
#
# def dt(date_str, format_str):
#     try:
#         return datetime.strptime(date_str, format_str)
#     except:
#         return None
# dt('14.01.2022', '%d.%m.%Y')
# print(dt('14.01.2022', '%d.%m.%Y'))
# assert dt('12:12:2021', '%h') == None
# assert dt('14.01.2022', '%d.%m.%Y') == datetime(day=14, month=1, year=2022)

"""
HW5
"""

"""
1. Создать txt файл, в нем написать текст.
 - Считать данные с файла и вывести в консоль
 - Считать данные с этого же файла и записать в новый файл
 - Считать данные с этого же файла, преобразовать (любые операции) и записать в этот же файл с разделителем
   (пробел, точка, запятая и тп)
"""
# def open_new_file():
#
#     file = open('new.txt', 'w+')
#     file.write('Hello, Vitalii.\nIf you read this text, it means I have finished my HW')
#     file.close()
#
# open_new_file()
#
# def read_new_file():
#     with open('new.txt', 'r') as file:
#         print(file.read())
#
# read_new_file()
#
# def create_file():
#
#     with open('new_2.txt', 'w') as new_file, open('new.txt', 'r') as old_file:
#         new_file.write(old_file.read())
#
# create_file()
#
# def change_file():
#     with open ('new.txt', 'r') as file:
#         old_data = file.read()
#         new_data = old_data.replace(' ', '_')
#
#     with open ('new.txt', 'w') as file:
#       file.write(new_data)
#
# change_file()

"""
2. Преобразовать дату. Нужно написать код, который из Feb 12 2019 2:41PM сделает 2019-02-12 14:41:00
"""

# from datetime import datetime
#
# def change_data(date):
#
#     d = datetime.strptime(date, '%b %d, %Y, %I:%M%p')
#     print(d)
#     return date
#
# change_data('Jan 16, 2022, 2:52PM')


"""
3. Напишите программу, которая принимает год, и возвращает список дат всех понедельников в данном году.
   Работа с датами (можно использовать любые модули и гуглить)
"""
# import calendar
#
# def main(year):
#     data = []
#     for month in range(1, 13):
#         for weak in calendar.Calendar().monthdatescalendar(year=year, month=month):
#             for day in weak:
#                 if day.isoweekday() == 1 and day.year == year:
#                     data.append(day.strftime('%d.%m.%Y'))
#     for d in data:
#         print(d)
#     return d
#
# main(2021)

"""
HW6
"""
"""
Взлом пароля
"""

# import string
# import random
# import zipfile

# PASSWORD_LENGTH = 4

# def extract_archive(file, password):
#     """
#     Функция открывает архив с паролем и возвращает результат операции (bool)
#     """
#     try:
#         file.extractall(pwd=password.encode())
#         return True
#     except Exception as e:
#         print(e)
#         return False

# def build_pass():
#     empty = ''
#     for _ in range(PASSWORD_LENGTH):
#         empty += random.choice(string.digits)
#     return empty

# def hack_archive():
#     """
#     Функция брутфорсит запароленный архив
#     """
#     wrong_passwords = []  # список паролей, которые не подошли
#     tries = 0  # колличество неудачных попыток
#     file_to_open = zipfile.ZipFile('task.zip', 'r')  # объект архива
    
#     while True:
#         pwd = build_pass()
#         if pwd in wrong_passwords:
#             continue
#         if extract_archive(file_to_open, password=pwd):
#             print(f'Archive {file_to_open} is hacked. Password - {pwd}')
#             print(f'Password was found after {tries} tries')
#             break
#         else:
#             wrong_passwords.append(pwd)
#             tries += 1
# hack_archive()

"""
HW7
"""
"""
1. Программа должна запрашивать у пользователя сумму в гривнах (uah), и тип валюты (usd/euro) для обмена.
Если валюта не из списка, печатаем ошибку.
Если все ок, печатаем сумму в запрашиваемой валюте по курсу.
"""
# def rate_of_exchange():
#     while True:
#         try:
#             a = float(input('Enter your amount in UAH: '))
#             break
#         except ValueError:
#             print('Enter number')
#         continue
#     while True:
#         try:
#             b = int(input('Select currency: USD - 1, EUR - 2: '))
#             rate = {'USD': 29.00, 'EUR': 32.7}
#             if b == 1:
#                 c = rate['USD']
#                 d = a / c
#                 print(round(d, 2), 'USD')
#                 break
#             elif b == 2:
#                 c = rate['EUR']
#                 d = a / c
#                 print(round(d, 2), 'EUR')
#                 break
#             else:
#                 print('Incorrect data')
#                 continue
#         except ValueError:
#             print('Enter correct number')
#             continue

# rate_of_exchange()
"""
2. Написать функцию, которая на вход принимает число и возвращает сумму всех его цифр. 
Операцию повторять до тех пор, пока не останется одна цифра.
Например:
дано: 5349
5 + 3 + 4 + 9 = 21 2 + 1= 3
вывод: 3
"""

# from functools import reduce

# def sum(num):
#     m = [int(a) for a in str(num)]

#     if len(m) == 1:
#         return num
#     else:
#         res = reduce(lambda x, y: x + y, m)
#     return sum(res)

# print(sum(9866))

"""
3. Написать функцию для сортировки для  списка словарей.
Сортировать по ключу `name`, если такого ключа нету в словаре, то по ключу `lastname`
Пример словаря - {'name': 'Ivan', 'lastname': 'Ivanov'}
"""
# def sort():
#     list_dict = [{'name': 'Ivan', 'lastname': 'Ivanov'}, {'name': 'Sergei', 'lastname': 'Sergeev'},
#                  {'name': 'Alex', 'lastname': 'Alexandrov'}, {'name': 'Petr', 'lastname': 'Petrov'}]
#     newlist = sorted(list_dict, key=lambda k: k.get('name') or k.get('lastname'))
#     return newlist
# print(sort())

"""
HW8
"""
"""
Написать игру "Угадай число". Написать программу, которая "загадает" рандомное число от 1 до n.
    Есть 2 (и больше) учасников, которые должны отгадать это число, если число отгадано - выводить
    имя участника и попытку, с которой он отгадал число.
Правила:
 - писать все функциями
 - обязательно запускать код для проверки!
 - число загадывается 1 раз на все время исполнения программы
 - количество попыток устанавливать сомостоятельно
"""
# import random
# a = 0
# b = 10
#
# def number(a, b):
#     num = random.randint(a, b)
#     return num
#
# def number_of_players():
#     while True:
#         try:
#             m = int(input('Enter the number of players: '))
#             if m >= 2:
#                 break
#             else:
#                 print('Enter the number more 2')
#                 continue
#         except ValueError:
#             print('Enter correct number')
#             continue
#     return m
#
# def players_name():
#     name_list = []
#     m = number_of_players()
#     while True:
#         name = input('Enter the name of players: ')
#         name_list.append(name)
#         if len(name_list) == m:
#             break
#         elif name not in name_list:
#             break
#         else:
#             continue
#     return name_list
#
# def guess_number():
#     nl = players_name()
#     num = number(a,b)
#     p = 0
#     print(num)
#     while True:
#         p += 1
#         try:
#             for N in nl:
#                 i = int(input(f'{N}, enter the number from {a} to {b}: '))
#                 if i == num:
#                     print(f'Congratulations {N}, you have won. Try # {p}')
#                     return
#                 else:
#                     continue
#         except ValueError:
#             print('Enter correct number')
#             continue
#     return i
# guess_number()
"""
HW8
"""
"""
Колобок
"""
# class Hero:
#     def __init__(self, name, phrase):
#         self.name = name
#         self.phrase = phrase

#     def dialogue(self):
#         print(f'{self.name}: {self.phrase}')
# fox1 = Hero('Fox', 'Здравствуй, колобок! Какой ты хорошенький. Колобок, колобок! Я тебя съем.')
# kolobok1 = Hero('Kolobok', 'Не ешь меня, лиса! Я тебе песенку спою: ля-ля-ля')
# fox2 = Hero('Fox', 'Какая славная песенка! Но ведь я, колобок, стара стала,'
#                    ' плохо слышу; сядь-ка на мою мордочку да пропой еще разок погромче.')
# fox3 = Hero('Fox', ' Спасибо, колобок! Славная песенка, еще бы послушала! '
#                    'Сядь-ка на мой язычок да пропой в последний разок,')
# fox1.dialogue()
# kolobok1.dialogue()
# fox2.dialogue()
# print('Колобок вскочил лисе на мордочку и запел ту же песню.')
# fox3.dialogue()
# print('колобок прыг ей на язык, а лиса — ам его! И съела колобка…')

"""
HW9
"""
"""
Крестики-Нолики
"""
# import random
# import typing as t

# class User(t.NamedTuple):
#     name: str
#     sign: str

# class XO:
#     __ALLOWED_SINGS = {'X', '0'}
#     __EMPTY = '_'

#     def __init__(self):
#         self.table = [
#             ['_', '_', '_'],
#             ['_', '_', '_'],
#             ['_', '_', '_'],
#         ]
#         self.table_map = {
#             '1': [0, 0],
#             '2': [0, 1],
#             '3': [0, -1],
#             '4': [1, 0],
#             '5': [1, 1],
#             '6': [1, -1],
#             '7': [-1, 0],
#             '8': [-1, 1],
#             '9': [-1, -1],
#         }

#         self.winner_coords = (
#             {'1', '2', '3'},
#             {'4', '5', '6'},
#             {'7', '8', '9'},
#             {'1', '4', '7'},
#             {'2', '5', '8'},
#             {'3', '6', '9'},
#             {'1', '5', '9'},
#             {'3', '5', '7'},
#         )

#         self.x_coords = set()
#         self.o_coords = set()

#     def make_mark(self, field_num, sign):
#         coords = self.table_map[field_num]
#         existing_sign = self.table[coords[0]][coords[-1]]
#         if existing_sign != self.__EMPTY:
#             print(f'Point already allocated with {existing_sign}')
#             return
#         self.table[coords[0]][coords[-1]] = sign
#         if sign == 'X':
#             self.x_coords.add(field_num)
#         else:
#             self.o_coords.add(field_num)

#     def is_end(self, user_sing):
#         for coord in self.winner_coords:
#             if user_sing == 'X':
#                 if coord.issubset(self.x_coords):
#                     return True
#             else:
#                 if coord.issubset(self.o_coords):
#                     return True

#     def __str__(self):
#         return f"{self.table[0][0]}\t | \t{self.table[0][1]}\t | \t{self.table[0][-1]}\n" \
#                f"{self.table[1][0]}\t | \t{self.table[1][1]}\t | \t{self.table[1][-1]}\n" \
#                f"{self.table[-1][0]}\t | \t{self.table[-1][1]}\t | \t{self.table[-1][-1]}\n"


# def main():
#     xo = XO()
#     first_user_name = input('Input your name')
#     second_user_name = input('Input your name')
#     user_list = [first_user_name, second_user_name]
#     x_user = random.choice(user_list)
#     print(f'First user name {x_user}')
#     user_list.pop(user_list.index(x_user))
#     users = (User(name=x_user, sign='X'), User(name=user_list[0], sign='0'))
#     is_first = False
#     count = 0
#     while True:
#         print(xo)
#         user = users[is_first]
#         print(f'Current user {user.name}')
#         number_of_point = input('Point number')
#         xo.make_mark(field_num=number_of_point, sign=user.sign)
#         count += 1
#         if count >= 3 and xo.is_end(user.sign):
#             print(f'{user.name} if winner')
#             print(xo)
#             return
#         is_first = not is_first


# main()

import random

def table():
    table = [
        ['1', '2', '3'],
        ['4', '5', '6'],
        ['7', '8', '9'],
    ]
    print(f"{table[0][0]}\t | \t{table[0][1]}\t | \t{table[0][-1]}\n" \
          f"{table[1][0]}\t | \t{table[1][1]}\t | \t{table[1][-1]}\n" \
          f"{table[-1][0]}\t | \t{table[-1][1]}\t | \t{table[-1][-1]}\n")
    return table

def table_map():
    table_map = {
            1: [0, 0],
            2: [0, 1],
            3: [0, -1],
            4: [1, 0],
            5: [1, 1],
            6: [1, -1],
            7: [-1, 0],
            8: [-1, 1],
            9: [-1, -1],
        }
    return table_map

def winner_coords():
    winner_coords = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
            [1, 4, 7],
            [2, 5, 8],
            [3, 6, 9],
            [1, 5, 9],
            [3, 5, 7],
    ]
    return winner_coords

def gamer(first_name, second_name):
    user_list = [first_name, second_name]
    x_user = random.choice(user_list)
    print(f'First user name {x_user}')
    if x_user == first_name:
        o_user = second_name
    else:
        o_user = first_name
    user_list = [x_user, o_user]
    return user_list

def game():

    ul = gamer(first_name=input('Input your name'), second_name=input('Input your name'))
    sign = ['X', 'O']
    user_sign = dict(zip(ul, sign))
    tm = table_map()
    ta = table()
    wc = winner_coords()
    x_list = []
    o_list = []
    while True:
        try:
            for N in ul:
                i = int(input(f'{N}, enter the field number: '))
                if user_sign[N] == 'X':
                    x_list.append(i)
                else:
                    o_list.append(i)
                coord = tm[i]
                ta[coord[0]][coord[-1]]=user_sign[N]
                print(f"{ta[0][0]}\t | \t{ta[0][1]}\t | \t{ta[0][-1]}\n" \
                      f"{ta[1][0]}\t | \t{ta[1][1]}\t | \t{ta[1][-1]}\n" \
                      f"{ta[-1][0]}\t | \t{ta[-1][1]}\t | \t{ta[-1][-1]}\n")
                if x_list in wc:
                    print(f'Congratulate {N}')
                    return
                elif o_list in wc:
                    print(f'Congratulate {N}')
                    return
        except ValueError:
            print('Enter correct number')
            continue

    return

game()
